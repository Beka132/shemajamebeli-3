package com.example.shemajamebeli3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Adapter
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_user.*
import kotlinx.android.synthetic.main.item_user.*

class UserActivity : AppCompatActivity() {
    private val users= mutableListOf<ItemModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        initRecyclerView()
        init()
    }
    private fun init(){
        val adapter=UserAdapter(users)
        recyclerView.adapter=adapter
        btnAdd.setOnClickListener{
            if(!users.contains(ItemModel(etFirstName.text.toString(),etLastName.text.toString(),etEmail.text.toString())))
                users.add(ItemModel(etFirstName.text.toString(),etLastName.text.toString(),etEmail.text.toString()))
            else Toast.makeText(this,"This user is alreadyon the list", Toast.LENGTH_SHORT).show()
        }
        btnRemove.setOnClickListener {
            if (users.contains(ItemModel(etFirstName.text.toString(),etLastName.text.toString(),etEmail.text.toString()))){
                val position=users.indexOf((ItemModel(etFirstName.text.toString(),etLastName.text.toString(),etEmail.text.toString())))
                users.removeAt(position)
                adapter.notifyItemRemoved(position)
            }else{
                Toast.makeText(this,"User does not exist",Toast.LENGTH_SHORT).show()
            }
        }
        btnUpdate.setOnClickListener{
            if (users.contains(etEmail.text.toString())){
                val position=users.indexOf(etEmail.text.toString())
                users[position]=ItemModel(etFirstName.text.toString(),etLastName.text.toString(),etEmail.text.toString())
            }else{
                Toast.makeText(this,"User does not exist",Toast.LENGTH_SHORT).show()
            }
        }

    }
    private fun initRecyclerView(){
        val adapter=UserAdapter(users)
        recyclerView.adapter=adapter
        recyclerView.layoutManager=LinearLayoutManager(this)
        setData()
    }

    private fun setData(){
        users.add(ItemModel("Beka","Dvali","Gmail.com"))
        users.add(ItemModel("Beka","Dvali","Gmail.com"))
        users.add(ItemModel("Beka","Dvali","Gmail.com"))

    }
}