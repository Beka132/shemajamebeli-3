package com.example.shemajamebeli3

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_user.view.*

class UserAdapter(private val users:MutableList<ItemModel>):
    RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.item_user,parent,false)
        return UserViewHolder(view)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.itemView.apply {
            tvName.text=users[position].firstName
            tvLastName.text=users[position].lastName
            tvEmail.text=users[position].email
        }
    }

    override fun getItemCount()=users.size

    inner class UserViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
    }
}